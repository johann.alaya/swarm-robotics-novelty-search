import sys
import time
from tkinter import *
from tkinter import messagebox
import threading


red= 'red'
red_hex= '#ff0000'

blue='blue'
blue_hex= '#0000ff'

black='black'
black_hex= '#000000'

yellow='yellow'
yellow_hex= '#ffff00'

list_str= []
list_hex= []
list_str.append(red)
list_hex.append(red_hex)
list_str.append(blue)
list_hex.append(blue_hex)
list_str.append(black)
list_hex.append(black_hex)
list_str.append(yellow)
list_hex.append(yellow_hex)


def trad_hex_to_rgb(hex_str):
    """
    prend la chaine hexa et la transforme en list rgb
    """
    if hex_str.startswith('#'):
        hex_str = hex_str[1:]
    return [int(hex_str[i:i + 2], 16) for i in range(0, len(hex_str), 2)]


def trad_str_to_rgb(nom):
    """
    prend la chaine de charactere et la transforme en list rgb
    et retourn 0,0,0 si n'existe pas
    """
    for i in range(len(list_str)):
        if nom == list_str[i]:
            return trad_hex_to_rgb(list_hex[i])
    return [0,0,0]


class View(threading.Thread):

    red= 'red\n'
    blue='blue\n'
    black='black\n'
    yellow='yellow\n'
    def __init__(self,arene):
        super(View,self).__init__()
        """
        int * int -> View
        Cette fonction initialise la fenetre sur laquelle le robot va s'afficher
        : param x: largeur de la fenetre
        : param y: heuteur de la fenetre
        """
        #initialisation des parametres
        self._x = arene._x
        self._y = arene._y
        self._objets = []
        self.arene = arene
        self.fini = False

    def run(self):
        self._fenetre = Tk()
        self._canvas = Canvas(self._fenetre, width = self._x, height = self._y, background = "grey")
        self._canvas.pack()
        self._Bouton_Quitter = Button(self._fenetre, text = "Quitter", command = self._fenetre.destroy) #creation du boutton quitter
        self._Bouton_Quitter.pack()
        self._fenetre.after(1,self.update_arene)
        self._fenetre.mainloop()

    def afficher_robot(self,robot):
        """
        Cette fonction affiche le robot sur le canevas
        : param robot : robot a afficher
        """
        # x0 = obstacle._x - obstacle._r
        # y0 = y0 - obstacle._r
        # r1 = obstacle._r
        # x1=x0+(2*r1)
        # y1=y0+(2*r1)
        x = robot.pos[0]
        y = robot.pos[1]
        dx = robot.direction[0]
        dy = robot.direction[1]
        if (self.arene.charge!=None and robot.energy == 0):
            self._objets.append(self._canvas.create_oval(x-8, y-8, x+8, y+8,fill = "gray"))
        else:
            self._objets.append(self._canvas.create_oval(x-8, y-8, x+8, y+8,fill = "blue"))
        self._objets.append(self._canvas.create_polygon(x+40*dx,y+40*dy,x+10*dy,y-10*dx,x-10*dy,y+10*dx))
        self._canvas.create_oval(x, y, x, y, outline='#00fffc')

    def afficher_station_charge(self,charge):
        x = charge.pos[0]
        y = charge.pos[1]
        self._objets.append(self._canvas.create_oval(x-12, y-12, x+12, y+12,fill = "red"))
        ### faire attention a coordonnee y qui vaudra self._y - y !!! (pour avoir affichage a l'endroit)

    def update_arene(self,dt=1):
        """
        Affichage de l'arene et de ce que contient l'arène """
        self.clear()                   #On efface d'abord ce qui etait affiche precedemment
        for r in self.arene.robots:
            self.afficher_robot(r)
        if self.arene.charge!=None:
            self.afficher_station_charge(self.arene.charge)
        self.update(dt)
        if not self.fini:
            self._fenetre.after(5,self.update_arene)



    def clear(self):
                """
                Cette fonction supprime tous les elements affiches sur l'arene
                """
                for e in self._objets:
                        self._canvas.delete(e)
                self._objets = []


    #dt en s
    def update(self, dt=1):
        self._canvas.pack()
        #time.sleep(dt)
        self._canvas.update()




class Arene:
    def __init__(self, x = 42, y = 42, z = 42,robot=None, obstacles=[],observers=[], fps=25):
        """
    int * int * int * Robot * Obstacle[] -> Arene
    L'arene contient le robot et les obstacles
    :param x: longueur de l'arène
    :param y: largeur de l'arène
    :param z: hauteur de l'arène
    """
        #inititialisation des paramètres de l'arene

        self._x = x
        self._y = y
        self._z = z
        self.robots = []
        self.charge = None
        self._obstacles = obstacles
        self._observers = []
        self._max_proximite=100


    def add_obs(self,observer):
        self._observers.append(observer)
        pass

    def ajout_Robot(self, robot):
        """
    On peut ajouter un robot dans l'arene
    :param robot: robot a ajouter dans l'arène
    ne retourne rien
    """
        self.robots.append(robot)
    
    def ajout_Charge(self, charge):
        """
    On peut ajouter un robot dans l'arene
    :param robot: robot a ajouter dans l'arène
    ne retourne rien
    """
        self.charge=charge

    def ajout_Obstacle(self,obstacle):
        """
    Permet d'ajouter un obstacle a notre liste d'obstacle
    :param obstacle: obstacle a ajouter
    """
        self._obstacles.append(obstacle)


    def boucle_actualiser(self, fps = 60):
        while True:
            if self.robots != None:
                self.update(1/fps)
            time.sleep(1/fps)

    #dt en s
    def update(self, dt=0.1):
        """
    Permet de mettre a jour l'arène et ses elements
    :param dt: temps en secondes represente le temps
    ecoule pendant update
    """
        t = 0.04                                #intervalle de temps par defaut (40 ms)
        if(dt <= t):                         #si le temps dt insere est inferieur a 40ms on prendra dt
            self.robots.update(dt)
            #for obs in self._observers:
            #    obs.update_arene(self,dt)
            if self.proximite() == 0:
                for obs in self._observers:
                    obs.arret(False)
        else:
            self.robots.update(t)
            #for obs in self._observers:
            #    obs.update_arene(self,t)
            self.update(dt-t)
            if self.proximite() == 0:
                for obs in self._observers:
                    obs.arret(False)
                       #on rappelle la fonction avec un dt plus petit



    def proximite(self):
        """
    Fonction qui permet au robot de changer de direction de manière aleatoire
    lorsqu'il est trop proche d'un obstacle
    """
        x_p = self._robot.x
        y_p = self._robot.y
        z_p = self._robot.z
        max = self._max_proximite #en cm
        res = 0.0
        while(res < max):
            if (x_p + self._robot._direction[0]*res < 0 or x_p + self._robot._direction[0]*res > self._x or y_p + self._robot._direction[1]*res < 0 or y_p + self._robot._direction[1]*res > self._y):
                return res
            for o in self._obstacles:
                if o.est_dans(x_p + self._robot._direction[0]*res, y_p + self._robot._direction[1]*res, z_p + self._robot._direction[2]*res):
                    return res
            res += 0.1
        return res

    def set_max_proximite(self,valeur):
        self._max_proximite=valeur                  #Fonction qui test si le veleur donner est egale a la valuer max de proximite

    def fin(self):
        for obs in self._observers:
            obs.arret(True)


class Robot:
    def __init__(self,pos=(0,0)):
        self._position = pos
        self._direction = [0,1]


if __name__ == "__main__":
    pass