import random, math, neat, os, fit, draw
from tkinter.constants import X
from math import sin
from math import cos
from math import atan
from math import sqrt
import matplotlib.pyplot as plt
import numpy as np

#import visualize
from plotneat import draw_net,plot_stats,plot_species
#from visualize import draw_net
fps=60
idcounter=0
frames_passed=0
sensor1range=25.0 #Senseur pour communiquer avec d'autres robots
sensor2range=10.0 #Senseur pour detecter les murs

gen = 0
table_novelty_searsh = []
table_fit = []
roh = 0.65

timestep = 5000

class Arena:
    size=300.0
    controller=None

    def __init__(self, n, net=None):
        self.robots=[]
        if net!=None:
            self.controller=AggregationController(self, Strategy(net))
        else:
            self.controller=AggregationController(self)
        for i in range(n):
            pos_x= float (random.randint(4,self.size-4))
            pos_y= float (random.randint(4,self.size-4))
            dir_x = float (random.randint(1,100)) / 100.0
            dir_y = 1-(dir_x**2)
            self.robots.append(Robot(pos_x,pos_y,dir_x,dir_y,self))
    
    def getfit(self):
        point = fit.bcmcl(self.robots, 4)
        return  fit.fit(self.robots), point

    def update(self):
        global frames_passed
        frames_passed+=1
        self.controller.update()
        self.detect_collisions()

    def detect_collisions(self):
        diam=self.robots[0].DIAMETER
        for i in range(len(self.robots)):
            for j in range(len(self.robots)):
                if i==j:
                    continue
                p1=self.robots[i].pos
                p2=self.robots[j].pos
                distance = math.dist(p1,p2)
                if distance <= diam:
                    #print("collision")
                    dx=p2[0]-p1[0]
                    dy=p2[1]-p1[1]
                    p2[0] = p2[0] + dx * (diam-distance)
                    p2[1] = p2[1] + dy * (diam-distance)
                    p1[0] = p1[0] - dx * (diam-distance)
                    p1[1] = p1[1] - dy * (diam-distance)

class Robot:
    id=0
    pos=(1.0,1.0)
    direction=(1.0,0.0)
    arena=None
    DPS_Gauche=0.0
    DPS_Droit=0.0
    WHEEL_CIRCUMFERENCE=128.8053 #le diamètre des roues est 41mm
    DIAMETER=8.0 #le diametre du robot est de 8 cm
    WHEEL_BASE_CIRCUMFERENCE=5.30 #espace entre les roues? a vérifier
    MOTOR_LEFT=1
    MOTOR_RIGHT=2

    def __init__(self, x, y, sx, sy, a):
        global idcounter
        self.pos=[x,y]
        self.direction=[sx,sy]
        self.arena=a
        self.id=idcounter
        idcounter+=1

    def set_motor_dps(self, port, dps):
        global frames_passed
        """
        Fixe la vitesse d'un moteur en nombre de degres par seconde
        :port: une constante moteur,  MOTOR_LEFT ou MOTOR_RIGHT (ou les deux MOTOR_LEFT+MOTOR_RIGHT).
        :dps: la vitesse cible en nombre de degres par seconde
        """
        #if (frames_passed % 100 == 0):
            #print("motor",dps)
            #print("dps_avant", self.DPS_Gauche,self.DPS_Droit,self.id)
        if (port == self.MOTOR_LEFT):
            self.DPS_Gauche = dps
        elif (port  == self.MOTOR_RIGHT):
            self.DPS_Droit  = dps
        elif (port == self.MOTOR_RIGHT+self.MOTOR_LEFT):
            self.DPS_Gauche = dps
            self.DPS_Droit  = dps
        #if (frames_passed % 100 == 0):
            #print("dps", self.DPS_Gauche,self.DPS_Droit,self.id)
    
    def update(self, dt):
        circonference_cm = self.WHEEL_CIRCUMFERENCE/10
        if self.DPS_Gauche == self.DPS_Droit:
            self.move(dt * self.DPS_Gauche * circonference_cm / 360)
        elif self.DPS_Gauche == -self.DPS_Droit:
            self.rotate(dt * self.DPS_Droit * (self.WHEEL_CIRCUMFERENCE / self.WHEEL_BASE_CIRCUMFERENCE))
        elif self.DPS_Gauche > 0 and  self.DPS_Droit > 0 and self.DPS_Gauche < self.DPS_Droit:
            self.rotate(dt * (self.DPS_Droit - self.DPS_Gauche) * (self.WHEEL_CIRCUMFERENCE / self.WHEEL_BASE_CIRCUMFERENCE)/2)
            self.move(dt * (self.DPS_Gauche+self.DPS_Droit)/2 * circonference_cm / 360)
        elif self.DPS_Gauche > 0 and  self.DPS_Droit > 0 and  self.DPS_Gauche > self.DPS_Droit:
            self.rotate(-dt * (self.DPS_Gauche - self.DPS_Droit) * (self.WHEEL_CIRCUMFERENCE / self.WHEEL_BASE_CIRCUMFERENCE)/2)
            self.move(dt * (self.DPS_Gauche+self.DPS_Droit)/2 * circonference_cm / 360)
        
    #def update(self, dt):
    #    dt_max = 0.2
    #    if dt < dt_max:
    #        self.update_aux(dt)
    #    else:
    #        self.update_aux(dt_max)
    #        self.update(dt - dt_max)
    
    def move(self, dist): #Fonction interne LE CONTROLLEUR NE DOIR PAS Y AVOIR ACCES DIRECTEMENT
        #print("move", dist)
        #print("pos avant", self.pos)
        r= self.DIAMETER/2.0
        new_x = self.pos[0] + self.direction[0]*dist
        new_y = self.pos[1] + self.direction[1]*dist
        self.pos[0] = min(max(r, new_x),self.arena.size-r)
        self.pos[1] = min(max(r, new_y),self.arena.size-r)
        #print("pos apres", self.pos)
        #wait = input("Pause")
        # if frames_passed % 100 == 0:
        #     print("Le robot ", self.id, "est en position ", "(", self.pos[0], ",", self.pos[1],")")
        #     self.sense()
        #Le monde est considéré torique pour l'instant

    def rotate(self, theta): #Fonction interne LE CONTROLLEUR NE DOIR PAS Y AVOIR ACCES DIRECTEMENT
        #print("rotate")
        trad = math.radians(theta) #A vérifier
        dx = self.direction[0]
        dy = self.direction[1]
        #print("direction avant rotate:", dx, dy, self.id)
        self.direction[0] = dx*math.cos(trad) - dy*math.sin(trad)
        self.direction[1] = dx*math.sin(trad) + dy*math.cos(trad)
        #print("direction apres rotate:", self.direction[0], self.direction[1], self.id)

    def sense(self):
        L1,L2=[25 for i in range(8)],[10 for i in range(8)]
        clustersize=1
        for r in self.arena.robots:
            if self.id == r.id:
                continue
            #Equation d'un cercle de rayon égale a la portée du senseur et centré sur le robot demandeur
            x = self.pos[0]
            y = self.pos[1]
            x2 = r.pos[0]
            y2 = r.pos[1]
            d = self.DIAMETER
            radius = d/2.0
            a = (x2-x)**2 + (y2-y)**2
            if (a <= (sensor1range+d)**2): #+d car on mesure a partir des bords du robot 
                clustersize+=1
                #Moitié supérieure ou inférieure du cercle ?
                if (y2 >= y):
                    #Moitié gauche ou droite du cercle ?
                    if (x2 >= x):
                        #Moitié supérieure ou inférieure du cadran ?
                        if (y2 - x2 <= y - x):
                            L1[0]=min(L1[0],math.dist((x,y),(x2,y2))-d)
                        else:
                            L1[1]=min(L1[1],math.dist((x,y),(x2,y2))-d)
                    else:
                        #Moitié supérieure ou inférieure du cadran ?
                        if (x2 + y2 >= x + y):
                            L1[2]=min(L1[2],math.dist((x,y),(x2,y2))-d)
                        else:
                            L1[3]=min(L1[3],math.dist((x,y),(x2,y2))-d)
                else: 
                    #Moitié gauche ou droite du cercle ?
                    if (x2 <= x):
                        #Moitié supérieure ou inférieure du cadran ?
                        if (y2 - x2 >= y - x):
                            L1[4]=min(L1[4],math.dist((x,y),(x2,y2))-d)
                        else:
                            L1[5]=min(L1[5],math.dist((x,y),(x2,y2))-d)
                    else:
                        #Moitié supérieure ou inférieure du cadran ?
                        if (x2 + y2 <= x + y):
                            L1[6]=min(L1[6],math.dist((x,y),(x2,y2))-d)
                        else:
                            L1[7]=min(L1[7],math.dist((x,y),(x2,y2))-d)
        for i in range(len(L1)):
            L2[i] = min(L2[i],L1[i])
        #Ajouter les murs
        #test mur droite
        L2[0] = min(L2[0], math.dist((x,y),(self.arena.size,y))-radius)
        #test mur gauche
        L2[3] = min(L2[3], math.dist((x,y),(0,y))-radius)
        #test mur haut
        L2[1] = min(L2[1], math.dist((x,y), (x,self.arena.size))-radius)
        #test mur bas
        L2[5] = min(L2[5], math.dist((x,y), (x,0))-radius)
        
        return L1,L2,clustersize
            

class Controller:
    arena=None
    strategy=None

    def __init__(self, a, s=None):
        self.arena=a
        self.strategy=s

    def update(self): #Methode virtuelle
        pass

class AggregationController(Controller):
    
    def __init__(self, a, s=None):
        Controller.__init__(self, a, s)

    def update(self):
        if (self.strategy==None):
            #Si y'a pas de strategie on fait juste foncer les robots
            for r in self.arena.robots:
                #print("Le robot", r.id, "est en position", r.pos)
                r.set_motor_dps(3,10) #On fait avancer le robot a sa vitesse max
                #L,_=r.sense()
                #for (a,_) in L:
                #    print("Le robot", r.id, "a détecté le robot", a)
                r.update(1/fps)

        else:
            for r in self.arena.robots:
                #print("Le robot", r.id, "est en position", r.pos)
                #Les trois outputs du réseau neuronal sont la vitesse des moteurs et une valeur spéciale dédiée
                #a faire s'arreter completement le robot. Les input sont les valeurs normalisées des senseurs.
                L1,L2,clustersize=r.sense()
                #les inputs sont normalisés
                L3=[*[L1[i]/25.0 for i in range(len(L1))],*[L2[i]/10.0 for i in range(len(L2))],clustersize/len(self.arena.robots),r.direction[0],r.direction[1]]
                #print(L3)
                motorleft,motorright,stop = self.strategy.strategize(tuple(L3))
                #print(motorleft,motorright,stop)
                if(stop>0.5):
                    #print("STOOOOOOP")
                    r.set_motor_dps(3,0)
                else:
                    r.set_motor_dps(1,motorleft*360.0)
                    r.set_motor_dps(2,motorright*360.0)
                    r.update(1/fps)

class Strategy:
    net=None

    def __init__(self,nt):
        self.net=nt

    def strategize(self, input):
        return self.net.activate(tuple(input))
        #return (random.randrange(0,360),random.randrange(0,360),random.randrange(0,100) / 100.0)

def main(genomes, config):
    global timestep
    global gen
    global roh
    global table_novelty_searsh
    global table_fit
    #affichage
    print("génération", gen)
    gen += 1
    nets = []
    #birds = []
    ge = []
    arenas = []
    for genome_id, genome in genomes:
        genome.fitness = 0  # start with fitness level of 0
        net = neat.nn.FeedForwardNetwork.create(genome, config)
        nets.append(net)
        #birds.append(Bird(230,350))
        ge.append(genome)
        arenas.append(Arena(7,net))
    #a=Arena(3)
    a = draw.Arene(300,300,0)
    view = draw.View(a)
    view.start()
    lr=arenas[0].robots
    for r in lr:
        a.ajout_Robot(r)
    #draw_net(config, ge[0], True)
    for n in range(timestep):
        for i in range(len(genomes)):
            arenas[i].update()
    p_max_fit = None
    plotfit = None
    max_fit = -1000
    for i in range(len(genomes)):
        fit, point = arenas[i].getfit()
        fit_tot = 0
        ldist = np.array([math.dist(list(point), list(val)) for val in table_novelty_searsh])
        if len(ldist) > 1:
            ldist /= float(max(ldist))
            fit_tot =(1-roh) * fit + roh * np.mean(ldist)
        else:
            fit_tot =(1-roh) * fit
        if max_fit < fit_tot:
            plotfit = fit
            p_max_fit = point
            max_fit = fit_tot
        ge[i].fitness = fit_tot
    if p_max_fit != None:
        table_fit.append(plotfit)
        table_novelty_searsh.append(p_max_fit)
    plt.scatter(range(len(table_fit)), table_fit)
    plt.show()

def run(config_file):
    global table_fit
    config = neat.config.Config(neat.DefaultGenome, neat.DefaultReproduction,
                         neat.DefaultSpeciesSet, neat.DefaultStagnation,
                         config_file)
    # Create the population, which is the top-level object for a NEAT run.
    pop = neat.Population(config)
    stats = neat.StatisticsReporter()
    pop.add_reporter(stats)
    pop.add_reporter(neat.Checkpointer(5))


    winner = pop.run(main,20)
    plt.scatter(range(len(table_fit)), table_fit)
    plt.show()
    net =neat.nn.FeedForwardNetwork.create(winner, config)
    ra = Arena(7,net)
    a = draw.Arene(300,300,0)
    view = draw.View(a)
    view.start()
    lr=ra.robots
    for r in lr:
        a.ajout_Robot(r)
    # Add a stdout reporter to show progress in the terminal.
    # show final stats

    #draw_net(config, winner, True)
    plot_stats(stats, ylog=False, view=True)
    plot_species(stats, view=True)
    #plot_stats(pop.statistics)
    #plot_species(pop.statistics)
    #draw_net(winner, view=True)

    print('\nBest genome:\n{!s}'.format(winner))
    while True: 
        ra.update()

if __name__ == '__main__':
    local_dir = os.path.dirname(__file__)
    config_path = os.path.join(local_dir, 'config.txt')
    run(config_path)

    # #test senseurs:
    # a=Arena(0)
    # r=Robot(50.0,50.0,0,0,a)
    # r0=Robot(56.0,53.0,0,0,a)
    # r1=Robot(52.0,55.0,0,0,a)
    # r2=Robot(46.0,60.0,0,0,a)
    # r3=Robot(44.0,52.0,0,0,a)
    # r4=Robot(41,48,0,0,a)
    # r5=Robot(49,45,0,0,a)
    # r6=Robot(54,45,0,0,a)
    # r7=Robot(59,48,0,0,a)
    # a.robots.append(r)
    # a.robots.append(r0)
    # a.robots.append(r1)
    # a.robots.append(r2)
    # a.robots.append(r3)
    # a.robots.append(r4)
    # a.robots.append(r5)
    # a.robots.append(r6)
    # a.robots.append(r7)
    # print(r.sense())
    # #test murs:
    # rm=Robot(295.0,295.0,0,0,a)
    # a.robots.append(rm)
    # print(rm.sense())

    # #test move:
    # a=Arena(5)
    # graphics=draw.Arene(300,300,0)
    # view = draw.View(graphics)
    # view.start()
    # lr=a.robots
    # for r in lr:
    #     graphics.ajout_Robot(r)
    # while True: 
    #     a.update()

    # #test rotation:
    # a=Arena(5)
    # graphics=draw.Arene(300,300,0)
    # view = draw.View(graphics)
    # view.start()
    # lr=a.robots
    # for r in lr:
    #     graphics.ajout_Robot(r)
    #     r.set_motor_dps(1,random.randint(1,360))
    #     r.set_motor_dps(2,random.randint(1,360))
    # while True: 
    #     for r in lr:
    #         r.update(1/fps)