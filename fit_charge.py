import math
import numpy as np
import os

def masscenter(lrobots):
    """
        Returns the center mass and the maximum x and y
    Arg:
        lrobots(list[Robot]) :  list of robot.
    
    Returns:
        res(tuple) : the centermass
        maxs(list) : the maximum to each coordonate
    """
    listpos = [robot.pos for robot in lrobots]
    res = [float(sum(y)) / len(y) for y in zip(*listpos)]
    maxs = list(map(max, zip(*listpos)))
    return res, maxs

def dist(masscenter, pos):
    return math.sqrt((masscenter[0]-pos[0])**2+(masscenter[1]-pos[1])**2)
    #return math.sqrt((((masscenter[0]-pos[0])/maxx)**2)+(((masscenter[1]-pos[1])/maxy)**2))

def allneighbors(robot, lrobots, dismax, maxx, maxy):
    return [bot for bot in lrobots if dist(robot.pos, bot.pos) <= dismax and bot != robot]

def nb_cluster(lrobots, dismax, maxs):
    lsolorobot = [r.pos for r in lrobots]
    maxx = maxs[0]
    maxy = maxs[1]
    res= 0
    for robot in lrobots:
        #print("robot in ", robot.pos)
        if robot.pos not in lsolorobot:
            #print(robot.pos, "not in", lsolorobot)
            continue
        lsolorobot.remove(robot.pos)
        #print("new cluster in ", robot.pos)
        res += 1
        lpossible = allneighbors(robot, lrobots, dismax, maxx, maxy)
        if not any(r.pos in lsolorobot for r in lpossible):
            continue
        while len(lpossible) != 0:
            robot2 = lpossible.pop(0)
            if robot2.pos in lsolorobot:
                #print("same cluster in ", robot2.pos)
                lsolorobot.remove(robot2.pos)
                lpossible.extend(allneighbors(robot2, lrobots, dismax, maxx, maxy))
                lpossible = list(set(lpossible))
    return res

def bcmcl(lrobots, maxdist):
    mass, maxs = masscenter(lrobots)
    ldist = np.array([dist(mass, robot.pos) for robot in lrobots])
    return (np.mean(ldist), nb_cluster(lrobots, maxdist, maxs))


def fit(lrobots):
    mass, maxs = masscenter(lrobots)
    ldist = np.array([dist(mass, robot.pos) for robot in lrobots])
    maxi = max(ldist)
    if maxi > 0:
        ldist /= maxi
    ldist = 1.0 - ldist
    return np.mean(ldist)

def chargeFit(lrobots,energytable):
    nbSurvivors = 0.0
    totalEnergy = 0.0
    for r in lrobots:
        if r.energy != 0.0:
            nbSurvivors+=1
    for i in range(len(energytable)):
        totalEnergy+=energytable[i][0]
    totalEnergy /= len(energytable) * 20000.0 #energie max
    #simplifié par rapport a l'article, on prend directement la moyenne des energies des robots
    return 0.9 * nbSurvivors + 0.1 * totalEnergy 

def bsimple(lrobots,energytable):
    nbSurvivors = 0.0
    totalEnergy = 0.0
    for r in lrobots:
        if r.energy != 0.0:
            nbSurvivors+=1
    i=0
    while i < len(energytable) and energytable[i][1] > 0:
        totalEnergy += (energytable[i][0] / energytable[i][1])
        i+=1
    totalEnergy /= i * 20000.0
    #simplifié par rapport a l'article car sauvegarder
    #l'énergie des robots a chaque pas de la simulation
    #est trop couteux
    return (nbSurvivors,totalEnergy) 
